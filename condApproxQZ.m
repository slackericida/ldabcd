% Compute an approcimation of the minimum conductance of the graph, in terms of upperbound and lowerbound
% using the QR decomposition for computing lambda2
function [ lowerBound, upperBound ] = condApproxQZ( A )

%A = gpuArray(A);
deg = sum(A,2);
D = diag(deg);
N = D^(-1/2)*A*D^(-1/2);

eigvalues = eig(N);
en2 = eigvalues(2);

lowerBound = 1-en2;     
upperBound = sqrt(2*(1-en2));

end
