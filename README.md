# README #

[MetaClust, new_PClist] = LDABCD(my_data, adjacencyMatFun, tau) is the main function to be called.

**INPUT:**
1. my_data: the generic Dataset to be processed
2. adjacencyMatFun: function for computing the adjacency matrix on the input dataset - it must be defined ad-hoc for each type of dataset processedà
3. tau: value for rescaling the values in the adjacency matrix

**OUTPUT:**
1. MetaClust: list of metaclusters identified
2. new_PClist: list of parameters configurations associated to each metacluster

### What is this repository for? ###

It implements the LD-ABCD algorithms, used for cluster and knowledge discovery. For further details on the algorithm, please refer to [An Agent-Based Algorithm exploiting Multiple Local Dissimilarities for Clusters Mining and Knowledge Discovery](http://arxiv.org/abs/1409.4988)

### Who do I talk to? ###

Filippo Bianchi -  slackericida@gmail.com