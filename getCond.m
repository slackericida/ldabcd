function cond = getCond(A, cut)
    outcut = setdiff(1:length(A),cut);
    num = sum(sum(A(cut,outcut)));
    as = sum(sum(A(cut,:)));
    asb = sum(sum(A(outcut,:)));
    a = as * asb;
    m = sum(sum(A))/2;
    den = a / (2 * m);
    cond = num / den;
end