function policies = eval_policy(N_clusters)
    % Return the policies of the agents:
    % 1 = Exploration
    % 2 = Exploitation
    
    global max_ratio N_agents;
    if (N_clusters == 0)
        policies = ones(1,N_agents);
    else
        policies = ones(1,N_agents);
        policies(1:round(N_agents*max_ratio)) = 2;
    end
end