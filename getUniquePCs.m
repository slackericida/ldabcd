function [new_PClist] = getUniquePCs(MetaClust, PClist, Araw)
% recompute the CQ on the metaclusters using the identified PCs and deleting repeated entries

new_PClist = {};

for i=1:size(MetaClust,1)
    MetaClust_i = MetaClust(i,:);
    PClist_i = PClist{i};
    PClist_i = unique(PClist_i(:,1:end-1),'rows');
    new_PClist_i = [];
    
    for j=1:size(PClist_i,1)
        PC_ij = PClist_i(j,:);
        [A, ~] = evalDistMatr( Araw, 1, PC_ij);
        [ lower_bound, upper_bound ] = condApproxPM( A, .1 );
        cond_ij = getCond(A, find(MetaClust_i));
        cq_ij = getQuality(cond_ij, lower_bound, upper_bound);
        new_PClist_i = [new_PClist_i; PC_ij, cq_ij];
    end
    
    new_PClist{i} = new_PClist_i;  
end

end