function [ metrics ] = genMetric( BIN, varargin )
%GENMETRIC: 
% The fucntion can be used in 2 different modalities:
% 1) metrics = genMetric(BIN,N,D) - Generate N vectors of length D extracted from the space [0,1]^D, sampled with a resolution BIN.
% 2) metrics = genMetric(BIN,metric) - Generate 1 vector applying a permutation to metric
%
% INPUT1:
% - BIN: sampling resolution in the metric space
% - N: number of the new metrics to be generated
% - D: dimension of the metric space
%
% INPUT2:
% - BIN: sampling resolution in the metric space
% - metric: metrics to apply permutation
%
% OUTPUT:
% - new_metrics: set of the new metrics generated

% modality 1
if(nargin == 3)
    N = varargin{1};
    D = varargin{2};
    metrics = zeros(N,D);
    i = 1;

    while(i<=N)
        m = (randi(BIN+1,[1,D])-1)/BIN;
        if(nnz(m) >= 2) % discard matric with all 0 or with 1 only element different from 0
            m = m/max(m); % normalize the metric
            metrics(i,:) = m;
            i = i+1;
        end

    end
    
% modality 2    
elseif(nargin == 2)
    metric_in = varargin{1};
    D = size(metric_in,2);
    
    if(nnz(metric_in) < 2)
        error('Metrica non valida')
        
    end
    
    while(1)
        ind = randi(D);
        new_metric = metric_in;
        new_metric(ind) = (randi(BIN+1,1)-1)/BIN;
        if(nnz(new_metric) >= 2 && ~isequal(new_metric, metric_in))
            metrics = new_metric;
            break
        end
    end
    
else
    disp('Error: wrong number of input elements');
end


end

