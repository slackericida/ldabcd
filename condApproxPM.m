% Compute an approcimation of the minimum conductance of the graph, in terms of upperbound and lowerbound
% using the power method for computing lambda2
function [ lowerBound, upperBound ] = condApproxPM( A, epsilon )

deg = sum(A,2);
D = diag(deg);
N = D^(-1/2)*A*D^(-1/2);
NI = N + eye(size(A));
t = ceil(epsilon^(-1)*log(size(A,1)/epsilon));
[v1, ~] = PM1(NI,t);
en2 = PM2(NI, t, v1);
lowerBound = 1-en2;     
upperBound = sqrt(2*(1-en2));

end

% PM1
function [v1, l1] = PM1(A, t)
    
u = round(rand(length(A),1))*2-1;
for i = 1:t
    u = A*u;
    u = u / norm(u);
end
    l1 = (u'*(A*u))/(u'*u);
    v1 = u/norm(u);
end


% PM2
% method for find the second eigenvalue of a matrix.
% WARNING: the eigenvalue is of the matrix MI-I not MI!
function [l2] = PM2(MI, t, v1)

    x = round(rand(length(MI),1))*2-1;
    x = x - dot(v1,x)*v1;
    for i=1:t
        x = MI*x;
        x = x / norm(x);
        x = x - dot(v1,x)*v1;
    end
    l2 = (x'*MI*x/(x'*x))-1;
end