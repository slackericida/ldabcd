function [ A, Aexp ] = evalDistMatr( Araw, tau, pc )
%EVALDISTMATR 
%
% Evaluate the distance matrix, given the connection matrix Araw, tau and the parameter configuration pc.

depth = size(Araw,3);

A = bsxfun(@times,Araw,reshape(pc,[1 1 depth]));
A = sum(A,3);

A = A./nnz(pc);
Aexp = exp(-tau * A);
Aexp(logical(eye(size(Aexp)))) = 0;

A = 1./(A.^2 + .01);
A(logical(eye(size(A)))) = 0;

end

