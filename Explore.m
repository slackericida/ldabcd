function [ clusters_PCs_i ] = Explore( Araw, metric, start_energy, tau, cq_threshold)
% EXPLORE
%
% Search for new clusters and pcs

N_data = size(Araw,1);
clusters_PCs_i = [];

% 1 - compute adjacency matrix and conductance's bounds
[ A, Aexp ] = evalDistMatr( Araw, tau, metric);
[ lower_bound, upper_bound ] = condApproxPM( A, .1 );
visited_nodes = zeros(1, N_data);  

while (nnz(Aexp) > 1)
    
    % 2 - Initialize the random walk with InitWalk
    energy = start_energy;
    [P, node] = initWalk(Aexp);
    visited_nodes(node) = 1;
    new_visited_nodes = zeros(1, N_data);
    new_visited_nodes(node) = 1;             
    cond_list = [];
    
    % 3 - Perform RW until the energy is depleted or until all nodes are visited
    while(energy > 0)

        % check if there are still nodes to visit
        if(sum(visited_nodes) == N_data)
            break;
        end

        % find the next node
        node = gendist(P(node,:),1,1);

        % update energy and the list of visited nodes
        if(new_visited_nodes(node))
            energy = energy - 0.5;                       
        else
            energy = energy - 1;
            new_visited_nodes(node) = 1; 
            visited_nodes(node) = 1;                         
        end

        % record how the conductance varies during the RW
        cond_list = [cond_list, getCond(A,find(new_visited_nodes==1))];  

        % energy update
%       if(length(cond_list) > 1) % 2 different conductance value must have been already evaluated
%           deltaCond = condList(end-1) -  condList(end);                
%           energyInc = 600*deltaCond*(lower_bound)+upper_bound);
%           energy = energy - 1 + energyInc; % <------------ CRITICAL
%       end              

    end

    % 4 - evaluate cluster
    num_nodes_clust = length(find(new_visited_nodes==1));
    cond = cond_list(end);
    cq = getQuality(cond, lower_bound, upper_bound);

    if (cq >= cq_threshold)
        % add the pc to the set of the best found so far, along with the cluster, the starting node and the cq
        clusters_PCs_i = [clusters_PCs_i; metric, new_visited_nodes, cq];
        speak('Cluster accettato. Conduttanza = ',cond,'. ClusterQuality = ',cq, ', # nodi: ',num_nodes_clust);
    else  
        speak('Cluster respinto. Conduttanza = ',cond,'. ClusterQuality = ',cq, ', # nodi: ',num_nodes_clust);
        break
    end

    Aexp(:, visited_nodes > 0) = 0;
    Aexp(visited_nodes > 0,:) = 0;
    
end

disp('Exploration: complete');

end

% ------- External Functions ------

function speak(varargin)
     disp(strcat(strjoin(cellfun(@num2str,varargin,'UniformOutput',false),'')))
end        
        
% Initialize the random walk  
function [P, node] = initWalk(Aexp)        

    % compute P e P_0 
    inv_D = diag(1./sum(Aexp,2));
    inv_D(inv_D == Inf) = 0;
    P = inv_D*Aexp;
    graph_size = sum(sum(Aexp));
    P_0 = (sum(Aexp,2))/graph_size;

    % compute the starting node
    node = gendist(P_0',1,1);

end


