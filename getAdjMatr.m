function [ Araw ] = getAdjMatr( DS )
% Compute the dissimilarity matrix
% This function is dataset dependant and must be rewritten when a new dataset is used
%
% INPUT
% - DS: dataset 
% - tau: treshold for attenuating the dissimilarity value (used in the RW)
% - pc: parameters (weights) of the dissimilarity measure
% - sogliaCut (optional): maximum length for an edge to be considered
%
% OUTPUT
% - A: adjacency matrix (used in the computation of the conductance)
% - Aexp: adjacency matric with rescaled distances

N = size(DS,1);
D = size(DS,2);

Araw = nan([N N D]);

% field 1
f = DS(:,1);
f_A = repmat(f,1,length(f));
f_B = repmat(f',length(f),1);
Araw(:,:,1) = f_A - f_B;

% field 2
f = DS(:,2);
f_A = repmat(f,1,length(f));
f_B = repmat(f',length(f),1);
Araw(:,:,2) = f_A - f_B;

% field 3
f = DS(:,3);
f_A = repmat(f,1,length(f));
f_B = repmat(f',length(f),1);
Araw(:,:,3) = f_A - f_B;



end

