function [results] = stimaTau(my_data, adjacencyMatFun)
% try different values of tau and record the average CQ obtained and the average dimension of the cluster during the RW

% init
N_data = size(my_data,1);
D = size(my_data,2);
metrics = ones(1,D);
cq_threshold = -inf;
results = [];

% variables
start_energy = round(N_data/2);
tau_max = 120;
tau_res = 2;
n_try = 100; % number of RW performed for each tau value
N_agents = 4;
wb = waitbar(0,'Please wait...','Name','LDABCD - stimaTau');

for i=1:round(tau_max/tau_res)
    tau = tau_res*i;
    res_i = [];
    
    for k=1:round(n_try/N_agents)
        
        clusters_PCs = cell(N_agents, 1);
        parfor j = 1:N_agents
            clusters_PCs{j} = Explore(my_data, metrics, start_energy, tau, cq_threshold, adjacencyMatFun);
        end

        for j=1:N_agents
            if(~isempty(clusters_PCs{j}))
                clusters_PCs_j = clusters_PCs{j};
                cluster_j = clusters_PCs_j(1:end-1,D+1:D+N_data); % non considero l'ultimo cluster perch� spesso sar� molto piccolo poich� fatto dai nodi rimanenti
                cq_j = clusters_PCs_j(1:end-1,end);
                res_i = [res_i; [cq_j, sum(cluster_j,2)]];
            end
        end
        
    end
       
    results = [results; [tau, mean(res_i(:,1)), std(res_i(:,1)), mean(res_i(:,2)), std(res_i(:,2))]];
    waitbar(i/(round(tau_max/tau_res)), wb, sprintf('%2.1f%%',i/(round(tau_max/tau_res))*100 ))
end
close(wb);

std_results = dataNorm( results, 2);
plot(std_results(:,2))
hold on
plot(std_results(:,4),'r')
legend('CQ','ClustSize')
hold off


end