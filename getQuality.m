function quality = getQuality(cond, lower, upper)
    quality = 1 - (cond - lower) / (upper - lower);
end