function [MetaClust, new_PClist] = LDABCD(my_data, adjacencyMatFun, tau)
%
% Main Method.
%
% INPUT:
%
% OUTPUT:
%

% Variables Definition
global max_ratio N_agents;
N_data = size(my_data,1);
N_agents = 4;
max_ratio = 1/4;
BIN = 1; % resolution of the search in the metric space
D = size(my_data,2); % number of parameters in the pc
start_energy = N_data*0.7; % initial energy  <------------ CRITICAL
cq_threshold = .7; % minimum Cluster Quality required for accepting the cluster found <------------ CRITICAL
tot_it = 100;
tic;
%energy_update = 100; % energy update parameter <------------ CRITICAL
% edge_tresh = 0; % threshold for pruning the adjacency graph (0 = not used)
% epsilon = 0.001; % precision for the computation of the eigenvalue within the power method technique
% stop = 0;

% Initialize data structures
MetaClust = [];
MCcard = [];
theta = .2;
PClist = cell(0);
Araw = adjacencyMatFun(my_data);

t=0;
wb = waitbar(0,'Please wait...','Name','LDABCD');
while(t < tot_it)
      
    % evaluate the policy
    policies = eval_policy(size(MetaClust,1));
    
    % generate the pcs
    metrics = genMetric(BIN, N_agents, D);
    
    % execute exploration or exploitation
    clusters_PCs = cell(N_agents, 1); % temporary container for the solutions found in the i-th iteration
    parfor i = 1:N_agents
        if(policies(i) == 1)
            clusters_PCs{i} = Explore(Araw, metrics(i,:), start_energy, tau, cq_threshold);
        else            
            exploit_ind = randi(size(MetaClust,1));
            clusters_PCs{i} = Exploit(MetaClust(exploit_ind,:), PClist{exploit_ind}, Araw, D, BIN);
        end
    end
        
    % join the solutions found in meta-clusters using teh BSAS
    for i = 1:N_agents

        if(~isempty(clusters_PCs{i}))
            clusters_PCs_i = clusters_PCs{i};
            for j=1:size(clusters_PCs_i,1)
                metric_ij = clusters_PCs_i(j,1:D);
                cluster_ij = clusters_PCs_i(j,D+1:D+N_data);
                cq_ij = clusters_PCs_i(j,end);
                [IDX, MetaClust, MCcard] = BSASonline(MetaClust, theta * sum(cluster_ij), cluster_ij, MCcard);
                MetaClust = round(MetaClust);
                if(length(PClist) < IDX)
                    PClist{IDX} = [metric_ij, cq_ij];
                else
                    PClist{IDX} = [PClist{IDX};[metric_ij, cq_ij]];
                end
            end
        end
        
    end
    
    % alternative stop criterion - TBD
    %stop = stopEval();
    
    t=t+1;
    waitbar(t / tot_it, wb, sprintf('%2.1f%%',t / tot_it*100))
    
end
close(wb)

[new_PClist] = getUniquePCs(MetaClust, PClist, Araw);

sprintf('Total time elapsed: %5.2f minutes.', toc/60)


end